'use strict';

angular.module('foodDiaryApp.admin', [
  'foodDiaryApp.auth',
  'ui.router'
]);
